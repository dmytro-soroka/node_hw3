const {
  SPRINTER,
  LARGE_STRAIGHT,
  SMALL_STRAIGHT,
} = require("../constants/truck.js");

const getTypeOfTruck = (type) => {
  switch (type) {
    case "SPRINTER":
      return SPRINTER;
    case "LARGE STRAIGHT":
      return LARGE_STRAIGHT;
    case "SMALL STRAIGHT":
      return SMALL_STRAIGHT;
  }
};

module.exports = getTypeOfTruck;
