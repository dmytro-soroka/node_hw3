const errorHandler = (err, _, res, _2) => {
  const errStatus = err.status ?? 500;
  const errMessage = err.message ?? "Error";
  return res.status(errStatus).json({ message: errMessage });
};

module.exports = errorHandler;
