const createNewLog = (message) => ({
  message: message,
  time: new Date().toISOString(),
});

module.exports = createNewLog;
