const express = require("express");
const {
  registerUser,
  loginUser,
  forgotPassword,
} = require("../controllers/authController.js");
const asyncWrapper = require("../utils/asyncWrapper.js");
const router = express.Router();

router.post("/register", asyncWrapper(registerUser));
router.post("/login", asyncWrapper(loginUser));
router.post("/forgot_password", asyncWrapper(forgotPassword));

module.exports = router;
