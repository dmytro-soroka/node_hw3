const express = require("express");
const {
  addTruck,
  assignTruck,
  updateTruckById,
  getTruckById,
  getTrucks,
  deleteTruckById,
} = require("../controllers/truckController.js");
const asyncWrapper = require("../utils/asyncWrapper.js");
const router = express.Router();

router.get("/", asyncWrapper(getTrucks));
router.post("/", asyncWrapper(addTruck));
router.post("/:id/assign", asyncWrapper(assignTruck));
router.put("/:id", asyncWrapper(updateTruckById));
router.get("/:id", asyncWrapper(getTruckById));
router.delete("/:id", asyncWrapper(deleteTruckById));

module.exports = router;
