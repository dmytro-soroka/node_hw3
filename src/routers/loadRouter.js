const express = require("express");
const {
  createLoad,
  getActiveLoad,
  getLoads,
  postLoadById,
  getShippingLoadInfo,
  deleteLoadById,
  updateLoadById,
  getLoadById,
  setLoadToNextStep,
} = require("../controllers/loadController.js");
const driverMiddleware = require("../middleware/driverMiddleware.js");
const shipperMiddleware = require("../middleware/shipperMiddleware.js");
const asyncWrapper = require("../utils/asyncWrapper.js");
const router = express.Router();

router.get("/", asyncWrapper(getLoads));
router.post("/", asyncWrapper(shipperMiddleware), asyncWrapper(createLoad));
router.post(
  "/:id/post",
  asyncWrapper(shipperMiddleware),
  asyncWrapper(postLoadById)
);
router.get(
  "/active",
  asyncWrapper(driverMiddleware),
  asyncWrapper(getActiveLoad)
);
router.get(
  "/:id/shipping_info",
  asyncWrapper(shipperMiddleware),
  asyncWrapper(getShippingLoadInfo)
);
router.delete(
  "/:id",
  asyncWrapper(shipperMiddleware),
  asyncWrapper(deleteLoadById)
);
router.put(
  "/:id",
  asyncWrapper(shipperMiddleware),
  asyncWrapper(updateLoadById)
);
router.get("/:id", asyncWrapper(getLoadById));
router.patch(
  "/active/state",
  asyncWrapper(driverMiddleware),
  asyncWrapper(setLoadToNextStep)
);

module.exports = router;
