const express = require("express");
const {
  getUserInfo,
  deleteUser,
  updatePassword,
} = require("../controllers/usersController");
const asyncWrapper = require("../utils/asyncWrapper");
const router = express.Router();

router.get("/", asyncWrapper(getUserInfo));
router.delete("/", asyncWrapper(deleteUser));
router.patch("/password", asyncWrapper(updatePassword));

module.exports = router;
