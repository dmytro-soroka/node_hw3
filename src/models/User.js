const mongoose = require("mongoose");
const Joi = require("joi");

const userJoiSchema = Joi.object({
  username: Joi.string().alphanum(),
  email: Joi.string()
    .email({
      minDomainSegments: 2,
    })
    .required(),
  password: Joi.string().min(3).alphanum().required(),
  role: Joi.string()
    .valid(...["DRIVER", "SHIPPER"])
    .required(),
});

const userSchema = mongoose.Schema({
  username: {
    type: String,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  role: {
    type: String,
    enum: ["DRIVER", "SHIPPER"],
    required: true,
  },
  created_date: {
    type: String,
    default: new Date().toISOString(),
  },
});

const User = mongoose.model("User", userSchema);

module.exports = { User, userJoiSchema };
