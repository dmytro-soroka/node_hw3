const Joi = require("joi");
const mongoose = require("mongoose");

const loginJoiSchema = Joi.object({
  email: Joi.string()
    .email({
      minDomainSegments: 2,
    })
    .required(),
  password: Joi.string().min(3).alphanum().required(),
});

const credentialSchema = mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

const Credential = mongoose.model("Credential", credentialSchema);

module.exports = { Credential, loginJoiSchema };
