const mongoose = require("mongoose");
const Joi = require("joi");

const registrationCredentialJoiSchema = Joi.object({
  password: Joi.string().min(3).alphanum().required(),
});

const registrationCredentialSchema = mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: ["DRIVER", "SHIPPER"],
    required: true,
  },
});

const RegistrationCredential = mongoose.model(
  "RegistrationCredential",
  registrationCredentialSchema
);

module.exports = { RegistrationCredential, registrationCredentialJoiSchema };
