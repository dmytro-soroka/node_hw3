const sgMail = require("@sendgrid/mail");
const bcrypt = require("bcryptjs");
const { User } = require("../models/User.js");
const { Credential } = require("../models/Credential.js");
const {
  RegistrationCredential,
} = require("../models/RegistrationCredential.js");

sgMail.setApiKey(
  "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"
);

const saveNewUser = async (user) => {
  const { email, role, username } = user;
  const newUser = new User({
    email,
    role,
    username,
  });
  return await newUser.save();
};

const saveCredential = async (user, _id) => {
  const { email, password } = user;
  const newCredential = new Credential({
    userId: _id,
    email,
    password: await bcrypt.hash(password, 10),
  });
  return await newCredential.save();
};

const saveRegCredential = async (user, _id) => {
  const { email, role, password } = user;
  const newCredential = new RegistrationCredential({
    userId: _id,
    email,
    role,
    password: await bcrypt.hash(password, 10),
  });
  return await newCredential.save();
};

const sendEmail = async (to, subject, text) => {
  const message = {
    to,
    from: "lololupik40@gmail.com",
    subject,
    text,
  };

  return await sgMail.send(message);
};

module.exports = {
  saveNewUser,
  saveCredential,
  saveRegCredential,
  sendEmail,
};
