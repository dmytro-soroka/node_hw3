const express = require("express");
const fs = require("fs");
const morgan = require("morgan");
const cors = require("cors");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const authRouter = require("./routers/authRouter.js");
const usersRouter = require("./routers/usersRouter.js");
const loadRouter = require("./routers/loadRouter.js");
const truckRouter = require("./routers/truckRouter.js");
const authMiddleware = require("./middleware/authMiddleware");
const errorHandler = require("./utils/errorHandler");
const asyncWrapper = require("./utils/asyncWrapper.js");
const driverMiddleware = require("./middleware/driverMiddleware.js");

require("dotenv").config();

const app = express();

const CONNECTION_URL =
  "mongodb+srv://dbUser:_Q3JYmvf.UN5h_4@dmytrosorokadatabase.ut4wkse.mongodb.net/loads?retryWrites=true&w=majority";

mongoose.connect(CONNECTION_URL);

const accessLogStream = fs.createWriteStream("src/logs/access.log", {
  flag: "a",
});

app.use(morgan("tiny", { stream: accessLogStream }));
app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use("/api/auth", authRouter);
app.use("/api/users/me", authMiddleware, usersRouter);
app.use("/api/loads", authMiddleware, loadRouter);
app.use(
  "/api/trucks",
  authMiddleware,
  asyncWrapper(driverMiddleware),
  truckRouter
);

app.listen(8080);

app.use(errorHandler);
