const TRUCK_TYPES = ["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT"];

const SPRINTER = {
  name: "SPRINTER",
  width: 300,
  length: 250,
  height: 170,
  capacity: 1700,
};

const SMALL_STRAIGHT = {
  name: "SMALL STRAIGHT",
  width: 500,
  length: 250,
  height: 170,
  capacity: 2500,
};

const LARGE_STRAIGHT = {
  name: "LARGE STRAIGHT",
  width: 700,
  length: 350,
  height: 200,
  capacity: 4000,
};

module.exports = {
  TRUCK_TYPES,
  SPRINTER,
  LARGE_STRAIGHT,
  SMALL_STRAIGHT,
};
