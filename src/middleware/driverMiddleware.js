const driverMiddleware = async (req, res, next) => {
  const { role } = req.user;

  if (role !== "DRIVER") throw new Error("No access permission");

  next();
};

module.exports = driverMiddleware;
