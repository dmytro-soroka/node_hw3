const jwt = require("jsonwebtoken");
const { User } = require("../models/User");

const authMiddleware = async (req, res, next) => {
  const { authorization } = req.headers;

  if (!authorization)
    return res
      .status(400)
      .json({ message: "Please, provide authorization header" });

  const token = authorization.split(" ")[1];

  if (!token)
    return res
      .status(400)
      .json({ message: "Please, include token to request" });

  const tokenPayload = jwt.verify(token, process.env.KEY);

  const user = await User.findById(tokenPayload.id);

  if (!user) return res.status(400).json({ message: "User doesn't exist" });

  req.user = {
    userId: user._id,
    role: user.role,
    email: user.email,
    created_date: user.created_date,
  };

  next();
};

module.exports = authMiddleware;
