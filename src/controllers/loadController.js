const { Load, loadJoiSchema } = require("../models/Load.js");
const createNewLog = require("../utils/createNewLog");
const { User } = require("../models/User.js");
const { Truck } = require("../models/Truck.js");
const getTypeOfTruck = require("../utils/getTypeOfTruck.js");
const getError = require("../utils/getError.js");

const createLoad = async (req, res, next) => {
  const { userId } = req.user;

  try {
    await loadJoiSchema.validateAsync(req.body);
  } catch (error) {
    return next(getError(400, error.message));
  }

  const load = new Load(req.body);

  load.created_by = userId;

  await load.save();

  res.status(200).json({
    message: "Load created successfully",
    load,
  });
};

const getLoads = async (req, res, next) => {
  const { status, limit, page } = req.query;
  const { userId, role } = req.user;

  const searchedParam =
    role === "SHIPPER" ? { createdBy: userId } : { assigned_to: userId };

  const result = await Load.find(searchedParam)
    .limit(+limit)
    .skip((+page - 1) * limit);

  const loads = status
    ? result.filter((load) => load.status === status)
    : result;

  res.status(200).json({
    loads,
    message: "You successfully get your loads",
  });
};

const getActiveLoad = async (req, res, next) => {
  const { userId } = req.user;

  const load = await Load.findOne({
    assigned_to: userId,
    status: "ASSIGNED",
  });

  if (!load) return next(getError(400, "You don't have assigned load"));

  res.status(200).json({
    message: "You successfully get your active load!",
    load,
  });
};

const postLoadById = async (req, res, next) => {
  const { id } = req.params;
  const { userId } = req.user;

  const load = await Load.findById(id);

  if (!load) return next(getError(400, "There are no loads with this id"));

  if (load.status === "ASSIGNED")
    return next(getError(400, "The load has already been assigned"));

  await load.updateOne({
    $set: {
      status: "POSTED",
    },
    $push: {
      logs: createNewLog("Load status changed to 'POSTED'"),
    },
  });

  let driver_found = false;

  const truck = await Truck.findOne({
    status: "IS",
    assigned_to: { $ne: null },
  });

  if (!truck) return next(getError(400, "No trucks"));

  const truckInfo = getTypeOfTruck(truck.type);

  const responseLoad = {
    ...load._doc,
    logs: [...load._doc.logs, createNewLog("Load status changed to 'POSTED'")],
  };

  if (
    truck &&
    load.payload < truckInfo.capacity &&
    load.dimensions.width < truckInfo.width &&
    load.dimensions.length < truckInfo.length &&
    load.dimensions.height < truckInfo.height
  ) {
    await truck.updateOne({
      $set: { status: "OL", shipperId: userId },
    });

    driver_found = true;

    await load.updateOne({
      $set: {
        status: "ASSIGNED",
        assigned_to: truck.created_by,
        state: "En route to Pick Up",
      },
      $push: {
        logs: createNewLog(
          `Load assigned to driver with id ${truck.created_by}`
        ),
      },
    });

    const newLoad = {
      ...responseLoad,
      status: "ASSIGNED",
      assigned_to: truck.created_by,
      state: "En route to Pick Up",
      logs: [
        ...responseLoad.logs,
        createNewLog(`Load assigned to driver with id ${truck.created_by}`),
      ],
    };

    res.status(200).json({
      message: "Load posted successfully",
      driver_found,
      load: newLoad,
    });
  } else {
    await load.updateOne({
      $set: {
        status: "NEW",
      },
      $push: {
        logs: createNewLog(`Load status changed to 'NEW'! Driver not found!`),
      },
    });

    const newLoad = {
      ...responseLoad,
      status: "NEW",
      logs: [
        ...responseLoad.logs,
        createNewLog(`Load status changed to 'NEW'! Driver not found!`),
      ],
    };

    res.status(200).json({
      message: "Driver not found",
      driver_found,
      load: newLoad,
    });
  }
};

const getShippingLoadInfo = async (req, res, next) => {
  const { id } = req.params;

  const load = await Load.findById(id);

  if (!load) return next(getError(400, "There is no such load"));
  if (load.status !== "ASSIGNED")
    return next(getError(400, "Load no assigned"));

  const truck = await Truck.findOne({ shipperId: load.created_by });
  const { shipperId, ...truckInfo } = truck._doc;

  res.status(200).json({ load, truck: truckInfo });
};

const deleteLoadById = async (req, res, next) => {
  const { id } = req.params;

  const load = await Load.findById(id);
  if (!load) return next(getError(400, "There is no load with such an Id"));
  const truck = await Truck.findOne({ shipperId: load.created_by });

  await load.deleteOne();

  if (truck)
    await truck.updateOne({
      $set: {
        status: "IS",
        shipperId: null,
      },
    });

  res.status(200).json({
    message: "Load deleted successfully",
    load,
  });
};

const updateLoadById = async (req, res, next) => {
  const { id } = req.params;

  const load = await Load.findById(id);
  if (!load || Object.keys(req.body).length === 0)
    return next(getError(400, "There is no load with such an Id"));

  const newLoad = { ...load._doc, ...req.body };
  await load.updateOne(req.body);

  res.status(200).json({
    message: "Load details changed successfully",
    load: newLoad,
  });
};

const getLoadById = async (req, res, next) => {
  const { id } = req.params;

  const load = await Load.findById(id);
  if (!load) return next(getError(400, "You do not have an load with this id"));

  res.status(200).json({ load });
};

const setLoadToNextStep = async (req, res, next) => {
  const { userId } = req.user;

  const driver = await User.findById(userId);

  const load = await Load.findOne({
    assigned_to: driver._id,
    status: "ASSIGNED",
  });

  if (!load) return next(getError(400, "You don't have a assigned load"));

  let setData;
  let responseMessage;
  const responseLoad = load._doc;

  switch (load.state) {
    case "En route to Pick Up":
      setData = { state: "Arrived to Pick Up" };
      responseMessage = "Load state changed to 'Arrived to Pick Up'";
      break;
    case "Arrived to Pick Up":
      setData = { state: "En route to delivery" };
      responseMessage = "Load state changed to 'En route to delivery'";
      break;
    case "En route to delivery":
      setData = { state: "Arrived to delivery", status: "SHIPPED" };
      responseMessage = "Load state changed to 'Arrived to delivery'";

      const truck = await Truck.findOne({ shipperId: load.created_by });

      await truck.updateOne({
        $set: {
          status: "IS",
          shipperId: null,
        },
      });
  }

  await load.updateOne({
    $set: setData,
    $push: {
      logs: createNewLog(responseMessage),
    },
  });

  res.status(200).json({
    message: responseMessage,
    load: {
      ...responseLoad,
      ...setData,
      logs: [...responseLoad.logs, createNewLog(responseMessage)],
    },
  });
};

module.exports = {
  createLoad,
  getLoads,
  getActiveLoad,
  postLoadById,
  getShippingLoadInfo,
  deleteLoadById,
  updateLoadById,
  getLoadById,
  setLoadToNextStep,
};
