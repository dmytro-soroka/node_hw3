const { Truck, trackJoiSchema } = require("../models/Truck.js");
const { TRUCK_TYPES } = require("../constants/truck.js");
const getError = require("../utils/getError.js");

const addTruck = async (req, res, next) => {
  const { userId } = req.user;

  try {
    await trackJoiSchema.validateAsync(req.body);
  } catch (error) {
    return next(getError(400, "Invalid information"));
  }

  const truck = new Truck(req.body);
  truck.created_by = userId;

  await truck.save();

  res.status(200).json({
    message: "Truck created successfully",
    truck,
  });
};

const assignTruck = async (req, res, next) => {
  const { id } = req.params;
  const { userId } = req.user;

  const truck = await Truck.findOne({
    _id: id,
    created_by: userId,
  });

  if (!truck) return next(getError(400, "No truck with this id"));
  if (truck.status === "OL") return next(getError(400, "This truck is busy"));

  const trucks = await Truck.find({ assigned_to: userId });

  if (trucks.length >= 1)
    return next(getError(400, "You already have assigned a truck"));

  await truck.updateOne({ $set: { assigned_to: userId } });

  const responseTruck = { ...truck._doc, assigned_to: userId };

  res.status(200).json({
    message: "Truck assigned successfully",
    truck: responseTruck,
  });
};

const getTrucks = async (req, res, next) => {
  const { userId } = req.user;

  const trucks = await Truck.find({ created_by: userId });

  res.status(200).json({
    message: "You successfully get your trucks",
    trucks,
  });
};

const getTruckById = async (req, res, next) => {
  const { id } = req.params;

  const truck = await Truck.findById(id);
  if (!truck)
    return next(getError(400, "You don't have a truck with such an Id"));

  res.status(200).json({ truck });
};

const updateTruckById = async (req, res, next) => {
  const { id } = req.params;
  const { type } = req.body;

  if (!TRUCK_TYPES.includes(type))
    return next(getError(400, "Undefined type of truck"));

  try {
    trackJoiSchema.validateAsync(req.body);
  } catch (error) {
    return next(400, error.message);
  }

  const truck = await Truck.findById(id);

  if (!truck || Object.keys(req.body).length === 0)
    return next(getError(400, "There is no truck with such an Id"));

  await truck.updateOne(req.body);

  const responseTruck = { ...truck._doc, type };

  res.status(200).json({
    message: "Truck details changed successfully",
    truck: responseTruck,
  });
};

const deleteTruckById = async (req, res) => {
  const { id } = req.params;

  const truck = await Truck.findByIdAndDelete(id);
  if (!truck) return next(getError(400, "You don't have truck with this id"));

  res.json({ message: "Truck deleted successfully", truck });
};

module.exports = {
  addTruck,
  assignTruck,
  getTrucks,
  getTruckById,
  updateTruckById,
  deleteTruckById,
};
