const generator = require("generate-password");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { User, userJoiSchema } = require("../models/User.js");
const { Credential, loginJoiSchema } = require("../models/Credential.js");
const {
  RegistrationCredential,
  registrationCredentialJoiSchema,
} = require("../models/RegistrationCredential.js");
const {
  saveCredential,
  saveNewUser,
  saveRegCredential,
  sendEmail,
} = require("../services/authService.js");
const getError = require("../utils/getError.js");

const registerUser = async (req, res, next) => {
  const { password } = req.body;

  try {
    await userJoiSchema.validateAsync(req.body);
    await registrationCredentialJoiSchema.validateAsync({
      password,
    });
  } catch (error) {
    return next(getError(400, error.message));
  }

  const { _id } = await saveNewUser(req.body);

  await saveRegCredential(req.body, _id);
  await saveCredential(req.body, _id);

  res.status(200).json({
    message: "Profile created successfully",
  });
};

const loginUser = async (req, res, next) => {
  const { email, password } = req.body;

  try {
    await loginJoiSchema.validateAsync(req.body);
    await registrationCredentialJoiSchema.validateAsync({
      password,
    });
  } catch (error) {
    return next(getError(400, error.message));
  }

  const currentUser = await User.findOne({ email });
  const currentCredential = await Credential.findOne({ email });

  if (!currentUser || !currentCredential)
    return next(getError(400, "There is no account with this login"));

  const isValidPassword = await bcrypt.compare(
    password,
    currentCredential.password
  );

  if (!isValidPassword) return next(getError(400, "Invalid password"));

  const jwt_token = jwt.sign({ id: currentUser._id }, process.env.KEY);

  res
    .status(200)
    .json({ jwt_token, email: currentUser.email, role: currentUser.role });
};

const forgotPassword = async (req, res, next) => {
  const { email } = req.body;

  if (!email) return next(getError(400, "Enter your email"));

  const credential = await Credential.findOne({ email });
  const regCredential = await RegistrationCredential.findOne({
    email,
  });

  if (!credential || !regCredential)
    return next(getError(400, "There is no account with this email"));

  const password = generator.generate({
    length: 5,
    numbers: true,
  });

  const passwordHash = await bcrypt.hash(password, 10);

  await credential.updateOne({ password: passwordHash });
  await regCredential.updateOne({
    password: passwordHash,
  });

  await sendEmail(email, "Your New password", `Password: ${password}`);

  res.status(200).json({
    message: "New password sent to your email address",
  });
};

module.exports = { registerUser, loginUser, forgotPassword };
