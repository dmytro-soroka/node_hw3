const bcrypt = require("bcryptjs");
const { Credential } = require("../models/Credential");
const { RegistrationCredential } = require("../models/RegistrationCredential");
const { User } = require("../models/User");
const getError = require("../utils/getError");

const getUserInfo = async (req, res, next) => {
  const { user } = req;
  res.status(200).json({ user });
};

const deleteUser = async (req, res, next) => {
  const { userId } = req.user;

  await User.findByIdAndDelete(userId);
  await Credential.findOneAndDelete({ userId });
  await RegistrationCredential.findOneAndDelete({ userId });

  res.status(200).json({
    message: "Profile deleted successfully",
  });
};

const updatePassword = async (req, res, next) => {
  const { email } = req.user;
  const { oldPassword, newPassword } = req.body;

  if (!oldPassword)
    return next(getError(400, "The old password field is mandatory"));
  if (!newPassword)
    return next(getError(400, "The new password field is mandatory"));

  const credential = await Credential.findOne({ email });
  const regCredential = await RegistrationCredential.findOne({
    email,
  });

  const isPasswordCorrect = await bcrypt.compare(
    oldPassword.toString(),
    credential.password.toString()
  );

  if (!isPasswordCorrect)
    return next(getError(400, "You have an incorrect password"));

  const password = await bcrypt.hash(newPassword, 10);

  await credential.updateOne({ password });
  await regCredential.updateOne({
    password,
  });

  res.status(200).json({
    message: "Password changed successfully",
  });
};

module.exports = {
  getUserInfo,
  deleteUser,
  updatePassword,
};
