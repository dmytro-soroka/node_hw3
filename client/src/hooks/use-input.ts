import { useReducer, ChangeEventHandler, useEffect } from "react";

interface IInputState {
  value: string;
  isTouched: boolean;
}

type TValidateValue = (val: string) => boolean;

type Action =
  | { type: "INPUT"; value: string }
  | { type: "BLUR" }
  | { type: "RESET" };

const initialInputState: IInputState = {
  value: "",
  isTouched: false,
};

const inputStateReducer = (state: IInputState, action: Action) => {
  switch (action.type) {
    case "INPUT":
      return { value: action.value, isTouched: state.isTouched };
    case "BLUR":
      return { value: state.value, isTouched: true };
    case "RESET":
      return initialInputState;
    default:
      return initialInputState;
  }
};

const useInput = (validateValue: TValidateValue, defaultValue?: string) => {
  const [inputState, dispatch] = useReducer(
    inputStateReducer,
    initialInputState
  );

  const valueIsValid = validateValue(inputState.value);
  const hasError = !valueIsValid && inputState.isTouched;

  const valueChangeHandler: ChangeEventHandler<HTMLInputElement> = (event) => {
    dispatch({ type: "INPUT", value: event.target.value });
  };

  useEffect(() => {
    if (defaultValue) {
      dispatch({ type: "INPUT", value: defaultValue });
    }
  }, [defaultValue]);

  const inputBlurHandler = () => {
    dispatch({ type: "BLUR" });
  };

  const reset = () => {
    dispatch({ type: "RESET" });
  };

  return {
    value: inputState.value,
    isValid: valueIsValid,
    hasError,
    valueChangeHandler,
    inputBlurHandler,
    reset,
  };
};

export default useInput;
