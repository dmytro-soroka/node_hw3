import { configureStore } from "@reduxjs/toolkit";
import authReducer from "./slices/authSlice";
import userReducer from "./slices/userSlice";
import trucksReducer from "./slices/trucksSlice";
import loadsReducer from "./slices/loadsSlice";

export const store = configureStore({
  reducer: {
    auth: authReducer,
    user: userReducer,
    loads: loadsReducer,
    trucks: trucksReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
