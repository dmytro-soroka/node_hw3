import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import truckApi from "../../api/truckApi";
import { ITruck, TTypeOfTruck } from "../../types/trucks";
import { RootState } from "../store";

interface IInitialState {
  trucks: ITruck[];
  isNewlyLoading: boolean;
  isError: boolean;
  isSuccess: boolean;
  isLoading: boolean;
  message: string;
}

const initialState: IInitialState = {
  trucks: [],
  isNewlyLoading: true,
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: "",
};

export const addTruck = createAsyncThunk(
  "trucks/addTruck",
  async (data: TTypeOfTruck, thunkAPI) => {
    try {
      const state = thunkAPI.getState() as RootState;
      const token = state.auth.user!.jwt_token;
      const response = await truckApi.addUserTruck(data, token);

      return response.data;
    } catch (error: any) {
      return thunkAPI.rejectWithValue(
        error.response.data.message ?? error.message
      );
    }
  }
);

export const getTrucks = createAsyncThunk(
  "trucks/getTrucks",
  async (_, thunkAPI) => {
    try {
      const state = thunkAPI.getState() as RootState;
      const token = state.auth.user!.jwt_token;
      const response = await truckApi.getUserTrucks(token);

      return response.data;
    } catch (error: any) {
      return thunkAPI.rejectWithValue(
        error.response.data.message ?? error.message
      );
    }
  }
);

export const assignTruck = createAsyncThunk(
  "trucks/assignTruck",
  async (id: string, thunkAPI) => {
    try {
      const state = thunkAPI.getState() as RootState;
      const token = state.auth.user!.jwt_token;
      const response = await truckApi.assignTruck(id, token);

      return response.data
    } catch (error: any) {
      return thunkAPI.rejectWithValue(
        error.response.data.message ?? error.message
      );
    }
  }
);

export const updateTruck = createAsyncThunk(
  "trucks/updateTruck",
  async ({ id, type }: { id: string; type: TTypeOfTruck }, thunkAPI) => {
    try {
      const state = thunkAPI.getState() as RootState;
      const token = state.auth.user!.jwt_token;
      const response = await truckApi.updateTruckById(id, type, token);

      return response.data;
    } catch (error: any) {
      return thunkAPI.rejectWithValue(
        error.response.data.message ?? error.message
      );
    }
  }
);

export const deleteUserTruckById = createAsyncThunk(
  "trucks/deleteUserTruckById",
  async (id: string, thunkAPI) => {
    try {
      const state = thunkAPI.getState() as RootState;
      const token = state.auth.user!.jwt_token;
      const response = await truckApi.deleteUserTruckById(id, token);

      return response.data;
    } catch (error: any) {
      return thunkAPI.rejectWithValue(
        error.response.data.message ?? error.message
      );
    }
  }
);

export const trucksSlice = createSlice({
  name: "trucks",
  initialState,
  reducers: {
    reset: (state) => {
      state.isLoading = false;
      state.isSuccess = false;
      state.isError = false;
      state.message = "";
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(addTruck.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(addTruck.fulfilled, (state, action) => {
        const { message, truck } = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.message = message;
        state.trucks = [...state.trucks, truck];
      })
      .addCase(addTruck.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload as string;
      })
      .addCase(getTrucks.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getTrucks.fulfilled, (state, action) => {
        const { message, trucks } = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.isNewlyLoading = false;
        state.message = message;
        state.trucks = trucks;
      })
      .addCase(getTrucks.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload as string;
      })
      .addCase(assignTruck.fulfilled, (state, action) => {
        const { message, truck } = action.payload;
        state.isSuccess = true;
        state.message = message;
        state.trucks = state.trucks.map((truckEl) =>
        truckEl._id === truck._id ? truck : truckEl
      );
      })
      .addCase(assignTruck.rejected, (state, action) => {
        state.isError = true;
        state.message = action.payload as string;
      })
      .addCase(updateTruck.fulfilled, (state, action) => {
        const { message, truck } = action.payload;
        state.isSuccess = true;
        state.message = message;
        state.trucks = state.trucks.map((truckEl) =>
          truckEl._id === truck._id ? truck : truckEl
        );
      })
      .addCase(updateTruck.rejected, (state, action) => {
        state.isError = true;
        state.message = action.payload as string;
      })
      .addCase(deleteUserTruckById.fulfilled, (state, action) => {
        const { message, truck } = action.payload;
        state.isSuccess = true;
        state.message = message;
        state.trucks = state.trucks.filter(
          (truckEl) => truckEl._id !== truck._id
        );
      })
      .addCase(deleteUserTruckById.rejected, (state, action) => {
        state.isError = true;
        state.message = action.payload as string;
      });
  },
});

export const selectTrucks = (state: RootState) => state.trucks;

export const { reset } = trucksSlice.actions;
export default trucksSlice.reducer;
