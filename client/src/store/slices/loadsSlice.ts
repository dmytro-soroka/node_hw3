import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import loadApi from "../../api/loadApi";
import { RootState } from "../store";
import { ILoad, ILoadCreate } from "../../types/loads";

interface IInitialState {
  loads: ILoad[];
  assignedLoad: ILoad | null;
  isNewlyGet: boolean;
  isNewlyGetAssignedLoad: boolean;
  isError: boolean;
  isSuccess: boolean;
  isLoading: boolean;
  message: string;
}

const initialState: IInitialState = {
  loads: [],
  assignedLoad: null,
  isNewlyGet: true,
  isNewlyGetAssignedLoad: true,
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: "",
};

export const addLoad = createAsyncThunk(
  "loads/addLoad",
  async (data: ILoadCreate, thunkAPI) => {
    try {
      const state = thunkAPI.getState() as RootState;
      const token = state.auth.user!.jwt_token;
      const response = await loadApi.createUserLoad(data, token);

      return response.data;
    } catch (error: any) {
      return thunkAPI.rejectWithValue(
        error.response.data.message ?? error.message
      );
    }
  }
);

export const getLoads = createAsyncThunk(
  "loads/getLoads",
  async (_, thunkAPI) => {
    try {
      const state = thunkAPI.getState() as RootState;
      const token = state.auth.user!.jwt_token;
      const response = await loadApi.getUserLoads(token);

      return response.data;
    } catch (error: any) {
      return thunkAPI.rejectWithValue(
        error.response.data.message ?? error.message
      );
    }
  }
);

export const getAssignedLoad = createAsyncThunk(
  "loads/getAssignedLoad",
  async (_, thunkAPI) => {
    try {
      const state = thunkAPI.getState() as RootState;
      const token = state.auth.user!.jwt_token;
      const response = await loadApi.getUserActiveLoad(token);

      return response.data;
    } catch (error: any) {
      return thunkAPI.rejectWithValue(
        error.response.data.message ?? error.message
      );
    }
  }
);

export const setNextStepLoad = createAsyncThunk(
  "loads/setNextStepLoad",
  async (_, thunkAPI) => {
    try {
      const state = thunkAPI.getState() as RootState;
      const token = state.auth.user!.jwt_token;
      const response = await loadApi.IterateNextState(token);

      return response.data;
    } catch (error: any) {
      return thunkAPI.rejectWithValue(
        error.response.data.message ?? error.message
      );
    }
  }
);

export const editLoad = createAsyncThunk(
  "loads/editLoad",
  async ({ id, data }: { id: string; data: ILoadCreate }, thunkAPI) => {
    try {
      const state = thunkAPI.getState() as RootState;
      const token = state.auth.user!.jwt_token;
      const response = await loadApi.updateLoad(id, data, token);

      return response.data;
    } catch (error: any) {
      return thunkAPI.rejectWithValue(
        error.response.data.message ?? error.message
      );
    }
  }
);

export const postLoad = createAsyncThunk(
  "loads/postLoad",
  async (id: string, thunkAPI) => {
    try {
      const state = thunkAPI.getState() as RootState;
      const token = state.auth.user!.jwt_token;
      const response = await loadApi.postUserLoadById(id, token);

      return response.data;
    } catch (error: any) {
      return thunkAPI.rejectWithValue(
        error.response.data.message ?? error.message
      );
    }
  }
);

export const deleteLoad = createAsyncThunk(
  "loads/deleteLoad",
  async (id: string, thunkAPI) => {
    try {
      const state = thunkAPI.getState() as RootState;
      const token = state.auth.user!.jwt_token;
      const response = await loadApi.deleteUserLoadById(id, token);

      return response.data;
    } catch (error: any) {
      return thunkAPI.rejectWithValue(
        error.response.data.message ?? error.message
      );
    }
  }
);

export const loadsSlice = createSlice({
  name: "loads",
  initialState,
  reducers: {
    reset: (state) => {
      state.isLoading = false;
      state.isSuccess = false;
      state.isError = false;
      state.message = "";
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(addLoad.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(addLoad.fulfilled, (state, action) => {
        const { message, load } = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.message = message;
        state.loads.push(load);
      })
      .addCase(addLoad.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload as string;
      })
      .addCase(getLoads.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getLoads.fulfilled, (state, action) => {
        const { message, loads } = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.isNewlyGet = false;
        state.message = message;
        state.loads = loads;
      })
      .addCase(getLoads.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload as string;
      })
      .addCase(getAssignedLoad.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getAssignedLoad.fulfilled, (state, action) => {
        const { message, load } = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.isNewlyGetAssignedLoad = false;
        state.message = message;
        state.assignedLoad = load;
      })
      .addCase(getAssignedLoad.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isNewlyGetAssignedLoad = false;
        state.message = action.payload as string;
      })
      .addCase(setNextStepLoad.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(setNextStepLoad.fulfilled, (state, action) => {
        const { message, load } = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.message = message;
        state.assignedLoad = load.status === "SHIPPED" ? null : load;
        state.loads = state.loads.map((loadEl) =>
          loadEl._id === load._id ? load : loadEl
        );
      })
      .addCase(setNextStepLoad.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload as string;
      })
      .addCase(editLoad.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(editLoad.fulfilled, (state, action) => {
        const { message, load } = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.message = message;
        state.loads = state.loads.map((loadEl) =>
          loadEl._id === load._id ? load : loadEl
        );
      })
      .addCase(editLoad.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload as string;
      })
      .addCase(postLoad.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(postLoad.fulfilled, (state, action) => {
        const { message, load } = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.message = message;
        state.loads = state.loads.map((loadEl) =>
          loadEl._id === load._id ? load : loadEl
        );
      })
      .addCase(postLoad.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload as string;
      })
      .addCase(deleteLoad.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(deleteLoad.fulfilled, (state, action) => {
        const { message, load } = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.message = message;
        state.loads = state.loads.filter((loadEl) => loadEl._id !== load._id);
      })
      .addCase(deleteLoad.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload as string;
      });
  },
});

export const selectLoads = (state: RootState) => state.loads;

export const { reset } = loadsSlice.actions;
export default loadsSlice.reducer;
