import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import userApi from "../../api/usersApi";
import { RootState } from "../store";
import { IChangePassData } from "../../types/api";

interface IInitialState {
  isError: boolean;
  isSuccess: boolean;
  isLoading: boolean;
  message: string;
}

const initialState: IInitialState = {
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: "",
};

export const changePassword = createAsyncThunk(
  "user/changePassword",
  async (data: IChangePassData, thunkAPI) => {
    try {
      const state = thunkAPI.getState() as RootState;
      const token = state.auth.user!.jwt_token;
      const response = await userApi.changePassword(data, token);

      return response.data;
    } catch (error: any) {
      return thunkAPI.rejectWithValue(
        error.response.data.message ?? error.message
      );
    }
  }
);

export const deleteUser = createAsyncThunk(
  "user/delete",
  async (_, thunkAPI) => {
    try {
      const state = thunkAPI.getState() as RootState;
      const token = state.auth.user?.jwt_token;
      if (!token) return thunkAPI.rejectWithValue("Something error");
      const response = await userApi.deleteUser(token);

      return response.data;
    } catch (error: any) {
      return thunkAPI.rejectWithValue(
        error.response.data.message ?? error.message
      );
    }
  }
);

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    reset: (state) => {
      state.isLoading = false;
      state.isSuccess = false;
      state.isError = false;
      state.message = "";
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(changePassword.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(changePassword.fulfilled, (state, action) => {
        const { message } = action.payload;
        state.isLoading = false;
        state.isSuccess = true;
        state.message = message;
      })
      .addCase(changePassword.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload as string;
      })
      .addCase(deleteUser.fulfilled, (state) => {
        localStorage.removeItem("user");
      })
      .addCase(deleteUser.rejected, (state, action) => {
        state.isError = true;
        state.message = action.payload as string;
      });
  },
});

export const selectUser = (state: RootState) => state.user;

export const { reset } = userSlice.actions;
export default userSlice.reducer;
