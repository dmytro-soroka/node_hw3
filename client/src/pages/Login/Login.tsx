import { useEffect, FormEvent } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import useInput from "../../hooks/use-input";
import { toast } from "react-toastify";
import { login, reset, selectAuth } from "../../store/slices/authSlice";
import isNotEmpty from "../../helpers/isNotEmpty";
import isEmail from "../../helpers/isEmail";
import LoadingButton from "@mui/lab/LoadingButton";
import {
  Avatar,
  TextField,
  Grid,
  Box,
  Typography,
  Container,
  Link as DecorLink,
  Button,
} from "@mui/material";
import { Link } from "react-router-dom";
import { AppDispatch } from "../../store/store";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";

const Login = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch<AppDispatch>();

  const {
    value: emailValue,
    isValid: emailIsValid,
    hasError: emailHasError,
    valueChangeHandler: emailChangeHandler,
    inputBlurHandler: emailBlurHandler,
    reset: resetEmail,
  } = useInput(isEmail);

  const {
    value: passwordValue,
    isValid: passwordIsValid,
    hasError: passwordHasError,
    valueChangeHandler: passwordChangeHandler,
    inputBlurHandler: passwordBlurHandler,
    reset: resetPassword,
  } = useInput(isNotEmpty);

  const formIsValid = emailIsValid && passwordIsValid;

  const { user, isLoading, isError, isSuccess, message } =
    useSelector(selectAuth);

  useEffect(() => {
    if (isError) toast.error(message);
    if (isSuccess) toast.success("You successfully login! Welcome =)");
    if (user) navigate("/");

    dispatch(reset());
  }, [user, isError, isSuccess, message, dispatch, navigate]);

  const submitHandler = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (!formIsValid) return;

    const userData = { email: emailValue, password: passwordValue };

    dispatch(login(userData));

    resetEmail();
    resetPassword();
  };

  const forgotPasswordHandler = () => {
    navigate("/forgot-password");
  };

  const emailLabel = emailHasError ? "theEmailMustHave@Symbol" : "email";
  const passwordLabel = passwordHasError
    ? "thePasswordCannotBeEmpty"
    : "password";

  return (
    <Container component="main">
      <Grid
        container
        flexDirection="column"
        alignItems="center"
        sx={{
          mt: 8,
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography variant="h5">{t("signIn")}</Typography>
        <Box
          noValidate
          component="form"
          onSubmit={submitHandler}
          sx={{ mt: 3, width: 400 }}
        >
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                value={emailValue}
                label={t(emailLabel)}
                error={emailHasError}
                onChange={emailChangeHandler}
                onBlur={emailBlurHandler}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                type="password"
                value={passwordValue}
                label={t(passwordLabel)}
                error={passwordHasError}
                onChange={passwordChangeHandler}
                onBlur={passwordBlurHandler}
              />
            </Grid>
          </Grid>
          <LoadingButton
            fullWidth
            type="submit"
            variant="contained"
            color="secondary"
            loading={isLoading}
            disabled={!formIsValid}
            sx={{ mt: 3, mb: 2 }}
          >
            {t("signIn")}
          </LoadingButton>
          <Button
            fullWidth
            variant="contained"
            color="info"
            sx={{ mb: 2 }}
            onClick={forgotPasswordHandler}
          >
            {t("forgotPassword")}
          </Button>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <DecorLink variant="body2">
                <Link to="/register">{t("youDontHaveAccountRegister")}</Link>
              </DecorLink>
            </Grid>
          </Grid>
        </Box>
      </Grid>
    </Container>
  );
};

export default Login;
