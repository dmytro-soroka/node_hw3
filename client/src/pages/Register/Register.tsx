import { useState, useEffect, FormEvent } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import useInput from "../../hooks/use-input";
import { toast } from "react-toastify";
import { register, reset, selectAuth } from "../../store/slices/authSlice";
import isNotEmpty from "../../helpers/isNotEmpty";
import isConfirmedPassword from "../../helpers/isConfirmedPassword";
import isEmail from "../../helpers/isEmail";
import LoadingButton from "@mui/lab/LoadingButton";
import {
  Avatar,
  TextField,
  Grid,
  Box,
  Typography,
  Container,
  Link as DecorLink,
  FormControlLabel,
  Radio,
  RadioGroup,
} from "@mui/material";
import { Link } from "react-router-dom";
import { AppDispatch } from "../../store/store";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";

const Register = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch<AppDispatch>();

  const [role, setRole] = useState("SHIPPER");

  const {
    value: emailValue,
    isValid: emailIsValid,
    hasError: emailHasError,
    valueChangeHandler: emailChangeHandler,
    inputBlurHandler: emailBlurHandler,
    reset: resetEmail,
  } = useInput(isEmail);

  const {
    value: passwordValue,
    isValid: passwordIsValid,
    hasError: passwordHasError,
    valueChangeHandler: passwordChangeHandler,
    inputBlurHandler: passwordBlurHandler,
    reset: resetPassword,
  } = useInput(isNotEmpty);

  const {
    value: confirmPasswordValue,
    isValid: confirmPasswordIsValid,
    hasError: confirmPasswordHasError,
    valueChangeHandler: confirmPasswordChangeHandler,
    inputBlurHandler: confirmPasswordBlurHandler,
    reset: resetConfirmPassword,
  } = useInput(isConfirmedPassword(passwordValue));

  const { user, isLoading, isError, isSuccess, message } =
    useSelector(selectAuth);

  const formIsValid = emailIsValid && passwordIsValid && confirmPasswordIsValid;

  useEffect(() => {
    if (isError) toast.error(message);
    if (isSuccess) {
      toast.success(message);
      navigate("/login");
    }
    if (user) navigate("/");

    dispatch(reset());
  }, [user, isError, isSuccess, message, dispatch, navigate]);

  const submitHandler = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (!formIsValid) return;

    const userData = { email: emailValue, password: passwordValue, role };

    dispatch(register(userData));

    resetEmail();
    resetPassword();
    resetConfirmPassword();
  };

  const radioButtonHandler = (event: any) => {
    setRole(event.target.value!);
  };

  const emailLabel = emailHasError ? "theEmailMustHave@Symbol" : "email";
  const passwordLabel = passwordHasError
    ? "thePasswordCannotBeEmpty"
    : "password";
  const confirmPasswordLabel = confirmPasswordHasError
    ? "passwordsMustMatch"
    : "confirmPassword";

  return (
    <Container component="main">
      <Grid
        container
        flexDirection="column"
        alignItems="center"
        sx={{
          mt: 8,
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography variant="h5">{t("signUp")}</Typography>
        <Box
          noValidate
          component="form"
          onSubmit={submitHandler}
          sx={{ mt: 3, width: 400 }}
        >
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                value={emailValue}
                label={t(emailLabel)}
                error={emailHasError}
                onChange={emailChangeHandler}
                onBlur={emailBlurHandler}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                type="password"
                value={passwordValue}
                label={t(passwordLabel)}
                error={passwordHasError}
                onChange={passwordChangeHandler}
                onBlur={passwordBlurHandler}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                type="password"
                value={confirmPasswordValue}
                label={t(confirmPasswordLabel)}
                error={confirmPasswordHasError}
                onChange={confirmPasswordChangeHandler}
                onBlur={confirmPasswordBlurHandler}
              />
            </Grid>
            <Grid item xs={12}>
              <Typography>Your role:</Typography>
              <RadioGroup defaultValue="SHIPPER" row>
                <FormControlLabel
                  value="SHIPPER"
                  control={<Radio />}
                  label={t("shipper")}
                  onChange={radioButtonHandler}
                />
                <FormControlLabel
                  value="DRIVER"
                  control={<Radio />}
                  label={t("driver")}
                  onChange={radioButtonHandler}
                />
              </RadioGroup>
            </Grid>
          </Grid>
          <LoadingButton
            fullWidth
            type="submit"
            variant="contained"
            color="secondary"
            loading={isLoading}
            disabled={!formIsValid}
            sx={{ mt: 3, mb: 2 }}
          >
            {t("signUp")}
          </LoadingButton>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <DecorLink variant="body2">
                <Link to="/login">{t("alreadyHaveAnAccountLogin")}</Link>
              </DecorLink>
            </Grid>
          </Grid>
        </Box>
      </Grid>
    </Container>
  );
};

export default Register;
