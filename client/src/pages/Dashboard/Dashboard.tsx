import React, { useState, SyntheticEvent } from "react";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { selectAuth } from "../../store/slices/authSlice";
import { Tabs, Tab, Box } from "@mui/material";
import NewLoad from "../../components/Tabs/NewLoad/NewLoad";
import AllTrucks from "../../components/Tabs/AllTrucks/AllTrucks";
import AssignedLoad from "../../components/Tabs/AssignedLoad/AssignedLoad";
import NewTruck from "../../components/Tabs/NewTruck/NewTruck";
import Profile from "../../components/Tabs/Profile/Profile";
import History from "../../components/Tabs/History/History";
import AllLoads from "../../components/Tabs/AllLoads/AllLoads";

interface ITabPanelParams {
  children?: React.ReactNode;
  index: number;
  value: number;
}

const TabPanel: React.FC<ITabPanelParams> = ({
  children,
  value,
  index,
  ...other
}) => (
  <Box
    role="tabpanel"
    hidden={value !== index}
    id={`vertical-tabpanel-${index}`}
    aria-labelledby={`vertical-tab-${index}`}
    sx={{ width: "100%" }}
  >
    {value === index && <Box sx={{ p: 3, width: "100%" }}>{children}</Box>}
  </Box>
);

const applyProps = (index: number) => ({
  "id": `vertical-tab-${index}`,
  "aria-controls": `vertical-tabpanel-${index}`,
  "sx": { alignItems: "start", paddingLeft: 4 },
});

const Dashboard = () => {
  const { t } = useTranslation();

  const [value, setValue] = useState(0);

  const { user } = useSelector(selectAuth);

  const isShipper = user?.role === "SHIPPER";

  const SHIPPER_PAGES = [
    { id: 0, page: <NewLoad />, label: "newLoad" },
    { id: 1, page: <AllLoads />, label: "allLoads" },
    { id: 2, page: <Profile />, label: "profile" },
    { id: 3, page: <History />, label: "history" },
  ];
  const DRIVER_PAGES = [
    { id: 0, page: <AllTrucks />, label: "allTrucks" },
    { id: 1, page: <AssignedLoad />, label: "assignedLoad" },
    { id: 2, page: <NewTruck />, label: "newTruck" },
    { id: 3, page: <Profile />, label: "profile" },
    { id: 4, page: <History />, label: "history" },
  ];

  const tabs = isShipper ? SHIPPER_PAGES : DRIVER_PAGES;

  const handleChange = (event: SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Box
      sx={{
        flexGrow: 1,
        display: "flex",
        height: "92.5vh",
        width: "100%",
      }}
    >
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        sx={{
          borderRight: 1,
          borderColor: "divider",
          width: 300,
          marginTop: 2,
        }}
      >
        {tabs.map((el) => (
          <Tab label={t(el.label)} {...applyProps(el.id)} />
        ))}
      </Tabs>
      {tabs.map((el) => (
        <TabPanel value={value} index={el.id}>
          {el.page}
        </TabPanel>
      ))}
    </Box>
  );
};

export default Dashboard;
