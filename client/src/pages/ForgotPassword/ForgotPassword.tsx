import { useEffect, FormEvent } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useTranslation } from "react-i18next";
import useInput from "../../hooks/use-input";
import { toast } from "react-toastify";
import {
  forgotPassword,
  reset,
  selectAuth,
} from "../../store/slices/authSlice";
import isEmail from "../../helpers/isEmail";
import LoadingButton from "@mui/lab/LoadingButton";
import {
  Avatar,
  TextField,
  Grid,
  Box,
  Typography,
  Container,
  Link as DecorLink,
} from "@mui/material";
import { Link } from "react-router-dom";
import { AppDispatch } from "../../store/store";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";

const ForgotPassword = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();

  const {
    value: emailValue,
    isValid: emailIsValid,
    hasError: emailHasError,
    valueChangeHandler: emailChangeHandler,
    inputBlurHandler: emailBlurHandler,
    reset: resetEmail,
  } = useInput(isEmail);

  const formIsValid = emailIsValid;

  const { isLoading, isError, isSuccess, message } = useSelector(selectAuth);

  useEffect(() => {
    if (isError) toast.error(message);
    if (isSuccess) toast.success("New password sended to your email!");

    dispatch(reset());
  }, [isError, isSuccess, message, dispatch]);

  const submitHandler = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (!formIsValid) return;

    dispatch(forgotPassword({ email: emailValue }));

    resetEmail();
  };

  const emailLabel = emailHasError ? "theEmailMustHave@Symbol" : "email";

  return (
    <Container component="main">
      <Grid
        container
        flexDirection="column"
        alignItems="center"
        sx={{
          mt: 8,
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography variant="h5">{t("forgotPassword")}</Typography>
        <Box
          noValidate
          component="form"
          onSubmit={submitHandler}
          sx={{ mt: 3, width: 400 }}
        >
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                value={emailValue}
                label={t(emailLabel)}
                error={emailHasError}
                onChange={emailChangeHandler}
                onBlur={emailBlurHandler}
              />
            </Grid>
          </Grid>
          <LoadingButton
            fullWidth
            type="submit"
            variant="contained"
            color="secondary"
            loading={isLoading}
            disabled={!formIsValid}
            sx={{ mt: 3, mb: 2 }}
          >
            {t("sendNewPassword")}
          </LoadingButton>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <DecorLink variant="body2">
                <Link to="/login">{t("rememberPasswordLogin")}</Link>
              </DecorLink>
            </Grid>
          </Grid>
        </Box>
      </Grid>
    </Container>
  );
};

export default ForgotPassword;
