import api from "./api";
import getApiConfig from "../helpers/getApiConfig";
import { IChangePassData } from "../types/api";

const userApi = {
  getUserInfo: (token: string) => api.get("/users/me", getApiConfig(token)),
  changePassword: (data: IChangePassData, token: string) =>
    api.patch("/users/me/password", data, getApiConfig(token)),
  deleteUser: (token: string) => api.delete("/users/me", getApiConfig(token)),
};

export default userApi;
