import getApiConfig from "../helpers/getApiConfig";
import { ILoad, ILoadCreate } from "../types/loads";
import api from "./api";

const loadApi = {
  createUserLoad: (data: ILoadCreate, token: string) =>
    api.post("/loads/", data, getApiConfig(token)),
  getUserActiveLoad: (token: string) =>
    api.get("/loads/active", getApiConfig(token)),
  getUserLoads: (token: string) => api.get("/loads/", getApiConfig(token)),
  postUserLoadById: (id: string, token: string) =>
    api.post(`/loads/${id}/post`, {}, getApiConfig(token)),
  getShippingInfo: (id: string, token: string) =>
    api.get(`/loads/${id}/shipping_info`, getApiConfig(token)),
  deleteUserLoadById: (id: string, token: string) =>
    api.delete(`/loads/${id}`, getApiConfig(token)),
  updateLoad: (id: string, data: ILoadCreate, token: string) =>
    api.put(`/loads/${id}`, data, getApiConfig(token)),
  getUserLoadById: (id: string, token: string) =>
    api.get(`/loads/${id}`, getApiConfig(token)),
  IterateNextState: (token: string) =>
    api.patch("/loads/active/state", {}, getApiConfig(token)),
};

export default loadApi;
