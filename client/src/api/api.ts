import axios, { AxiosInstance } from "axios";

const BASE_URL = "http://localhost:8080/api";

const api: AxiosInstance = axios.create({
  baseURL: BASE_URL,
  headers: {
    "Content-Type": "application/json",
  },
});

export default api;
