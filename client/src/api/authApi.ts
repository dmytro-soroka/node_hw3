import api from "./api";
import { IAuthData, IForgotPasswordData, IRegisterData } from "../types/api";

const authApi = {
  registerUser: (data: IRegisterData) => api.post("/auth/register", data),
  loginUser: (data: IAuthData) => api.post("/auth/login", data),
  forgotPassword: (data: IForgotPasswordData) =>
    api.post("/auth/forgot_password", data),
};

export default authApi;
