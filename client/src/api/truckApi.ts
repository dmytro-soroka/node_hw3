import getApiConfig from "../helpers/getApiConfig";
import { ITruck, TTypeOfTruck } from "../types/trucks";
import api from "./api";

const truckApi = {
  addUserTruck: (type: TTypeOfTruck, token: string) =>
    api.post("/trucks/", { type }, getApiConfig(token)),
  assignTruck: (id: string, token: string) =>
    api.post(`/trucks/${id}/assign`, {}, getApiConfig(token)),
  updateTruckById: (id: string, type: TTypeOfTruck, token: string) =>
    api.put(`/trucks/${id}`, { type }, getApiConfig(token)),
  getUserTruckById: (id: string, token: string) =>
    api.get(`/trucks/${id}`, getApiConfig(token)),
  getUserTrucks: (token: string) => api.get("/trucks", getApiConfig(token)),
  deleteUserTruckById: (id: string, token: string) =>
    api.delete(`/trucks/${id}`, getApiConfig(token)),
};

export default truckApi;
