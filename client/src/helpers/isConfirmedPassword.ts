const isConfirmedPassword = (firstPass: string) => (secondsPass: string) => firstPass === secondsPass;

export default isConfirmedPassword;
