const isValueBetweenCounts = (
  firstCount: number,
  secondCount: number,
  value: string
) => +value.trim() >= firstCount && +value.trim() <= secondCount;

export default isValueBetweenCounts;
