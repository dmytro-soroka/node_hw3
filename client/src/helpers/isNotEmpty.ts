const isNotEmpty = (value: string) => value.trim() !== "";

export default isNotEmpty;
