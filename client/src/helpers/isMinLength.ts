const isMinLength = (minLength: number, value: string) =>
  value.trim().length >= minLength;

export default isMinLength;
