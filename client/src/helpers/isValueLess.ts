const isValueLess = (count: number, value: string) =>
  +value < count && value.length > 0;

export default isValueLess;
