const isEmail = (value: string) => value.includes("@");

export default isEmail;
