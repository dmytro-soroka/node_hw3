const getApiConfig = (token: string) => ({
  headers: {
    Authorization: `Bearer ${token}`,
  },
});

export default getApiConfig;
