export interface IUser {
  role: string;
  jwt_token: string;
  email: string;
}
