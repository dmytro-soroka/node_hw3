export interface IAuthData {
  email: string;
  password: string;
}

export interface IRegisterData extends IAuthData {
  role: string;
}

export interface IForgotPasswordData {
  email: string;
}

export interface IChangePassData {
  oldPassword: string;
  newPassword: string;
}
