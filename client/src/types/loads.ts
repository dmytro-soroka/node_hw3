export type TLoadStatus = "NEW" | "POSTED" | "ASSIGNED" | "SHIPPED";

export type TNameOfLoad =
  | "En route to Pick Up"
  | "Arrived to Pick Up"
  | "En route to delivery"
  | "Arrived to delivery";

export interface IDimensions {
  width: number;
  length: number;
  height: number;
}

export interface ILog {
  message: string;
  time: string;
}

export interface ILoadCreate {
  name: string;
  payload: number;
  pickup_address: string;
  delivery_address: string;
  dimensions: IDimensions;
}

export interface ILoad {
  _id: string;
  created_by: string;
  assigned_to: string;
  status: TLoadStatus;
  state: TNameOfLoad;
  name: string;
  payload: number;
  pickup_address: string;
  delivery_address: string;
  dimensions: IDimensions;
  logs: ILog[];
  created_date: string;
}
