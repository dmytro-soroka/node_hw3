export interface IPage {
  name: string;
  path?: string;
  onClick?: () => void;
}
