export type TTypeOfTruck = "SPRINTER" | "SMALL STRAIGHT" | "LARGE STRAIGHT";

export type TStatusOfTruck = "OL" | "IS";

export interface ITruck {
  _id: string;
  created_by: string;
  assigned_to: string | null;
  type: TTypeOfTruck;
  status: TStatusOfTruck;
  created_date: string;
}
