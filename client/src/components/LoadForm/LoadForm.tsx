import React, { FormEvent } from "react";
import { useTranslation } from "react-i18next";
import useInput from "../../hooks/use-input";
import isMinLength from "../../helpers/isMinLength";
import isValueBetweenCounts from "../../helpers/isValueBetweenCounts";
import isValueLess from "../../helpers/isValueLess";
import { Box, Button, Grid, TextField, Typography } from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import { ILoad, ILoadCreate } from "../../types/loads";

interface ILoadFormParams {
  load?: ILoad;
  onSubmit: (data: ILoadCreate) => void;
  onClose?: () => void;
  title: string;
}

const defaultLoad = {
  name: "",
  payload: "",
  pickup_address: "",
  delivery_address: "",
  dimensions: {
    width: "",
    length: "",
    height: "",
  },
};

const LoadForm: React.FC<ILoadFormParams> = ({
  load = defaultLoad,
  onSubmit,
  onClose,
  title = "loadForm",
}) => {
  const { t } = useTranslation();

  const {
    value: nameValue,
    isValid: nameIsValid,
    hasError: nameHasError,
    valueChangeHandler: nameChangeHandler,
    inputBlurHandler: nameBlurHandler,
    reset: resetName,
  } = useInput(isMinLength.bind(null, 5), load.name);

  const {
    value: payloadValue,
    isValid: payloadIsValid,
    hasError: payloadHasError,
    valueChangeHandler: payloadChangeHandler,
    inputBlurHandler: payloadBlurHandler,
    reset: resetPayload,
  } = useInput(
    isValueBetweenCounts.bind(null, 10, 1000),
    load.payload.toString()
  );

  const {
    value: pickupAddressValue,
    isValid: pickupAddressIsValid,
    hasError: pickupAddressHasError,
    valueChangeHandler: pickupAddressChangeHandler,
    inputBlurHandler: pickupAddressBlurHandler,
    reset: resetPickupAddress,
  } = useInput(isMinLength.bind(null, 10), load.pickup_address);

  const {
    value: deliveryAddressValue,
    isValid: deliveryAddressIsValid,
    hasError: deliveryAddressHasError,
    valueChangeHandler: deliveryAddressChangeHandler,
    inputBlurHandler: deliveryAddressBlurHandler,
    reset: resetDeliveryAddress,
  } = useInput(isMinLength.bind(null, 10), load.delivery_address);

  const {
    value: widthValue,
    isValid: widthIsValid,
    hasError: widthHasError,
    valueChangeHandler: widthChangeHandler,
    inputBlurHandler: widthBlurHandler,
    reset: resetWidth,
  } = useInput(isValueLess.bind(null, 700), load.dimensions.width.toString());

  const {
    value: lengthValue,
    isValid: lengthIsValid,
    hasError: lengthHasError,
    valueChangeHandler: lengthChangeHandler,
    inputBlurHandler: lengthBlurHandler,
    reset: resetLength,
  } = useInput(isValueLess.bind(null, 350), load.dimensions.length.toString());

  const {
    value: heightValue,
    isValid: heightIsValid,
    hasError: heightHasError,
    valueChangeHandler: heightChangeHandler,
    inputBlurHandler: heightBlurHandler,
    reset: resetHeight,
  } = useInput(isValueLess.bind(null, 200), load.dimensions.height.toString());

  const formIsValid =
    nameIsValid &&
    payloadIsValid &&
    pickupAddressIsValid &&
    deliveryAddressIsValid &&
    widthIsValid &&
    lengthIsValid &&
    heightIsValid;

  const submitHandler = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (!formIsValid) return;

    const newLoad: ILoadCreate = {
      name: nameValue,
      payload: +payloadValue,
      pickup_address: pickupAddressValue,
      delivery_address: deliveryAddressValue,
      dimensions: {
        width: +widthValue,
        length: +lengthValue,
        height: +heightValue,
      },
    };

    onSubmit(newLoad);

    resetName();
    resetPayload();
    resetPickupAddress();
    resetDeliveryAddress();
    resetWidth();
    resetLength();
    resetHeight();
  };

  const nameLabel = nameHasError ? "theMinNameLengthMustBe6Characters" : "name";
  const payloadLabel = payloadHasError
    ? "theMinWeightIs10KgAndTheMaxIs1000"
    : "payload";
  const pickupAddressLabel = pickupAddressHasError
    ? "theMinLengthOfTheAddressMustBe10Characters"
    : "pickupAddress";
  const deliveryAddressLabel = deliveryAddressHasError
    ? "theMinLengthOfTheAddressMustBe10Characters"
    : "deliveryAddress";
  const widthLabel = widthHasError ? "theMaxWidthIs700" : "width";
  const lengthLabel = lengthHasError ? "theMaxLengthIs350" : "length";
  const heightLabel = heightHasError ? "theMaxHeightIs200" : "height";

  return (
    <Grid container flexDirection="column" alignItems="center">
      <Typography variant="h5">{t(title)}</Typography>
      <Box
        noValidate
        component="form"
        onSubmit={submitHandler}
        sx={{ mt: 3, width: 400 }}
      >
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              required
              fullWidth
              value={nameValue}
              label={t(nameLabel)}
              error={nameHasError}
              onChange={nameChangeHandler}
              onBlur={nameBlurHandler}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              required
              fullWidth
              value={payloadValue}
              label={t(payloadLabel)}
              error={payloadHasError}
              onChange={payloadChangeHandler}
              onBlur={payloadBlurHandler}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              required
              fullWidth
              value={pickupAddressValue}
              label={t(pickupAddressLabel)}
              error={pickupAddressHasError}
              onChange={pickupAddressChangeHandler}
              onBlur={pickupAddressBlurHandler}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              required
              fullWidth
              value={deliveryAddressValue}
              label={t(deliveryAddressLabel)}
              error={deliveryAddressHasError}
              onChange={deliveryAddressChangeHandler}
              onBlur={deliveryAddressBlurHandler}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              required
              fullWidth
              value={widthValue}
              label={t(widthLabel)}
              error={widthHasError}
              onChange={widthChangeHandler}
              onBlur={widthBlurHandler}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              required
              fullWidth
              value={lengthValue}
              label={t(lengthLabel)}
              error={lengthHasError}
              onChange={lengthChangeHandler}
              onBlur={lengthBlurHandler}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              required
              fullWidth
              value={heightValue}
              label={t(heightLabel)}
              error={heightHasError}
              onChange={heightChangeHandler}
              onBlur={heightBlurHandler}
            />
          </Grid>
        </Grid>
        <LoadingButton
          fullWidth
          type="submit"
          variant="contained"
          color="secondary"
          disabled={!formIsValid}
          sx={{ mt: 3, mb: 2 }}
        >
          {t(title)}
        </LoadingButton>
        {onClose && (
          <Button fullWidth variant="contained" onClick={onClose}>
            {t("cancel")}
          </Button>
        )}
      </Box>
    </Grid>
  );
};

export default LoadForm;
