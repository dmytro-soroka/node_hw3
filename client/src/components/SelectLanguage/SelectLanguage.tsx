import React from "react";
import { useTranslation } from "react-i18next";
import language from "../../translations/translation";
import {
  Box,
  FormControl,
  Select,
  MenuItem,
  SelectChangeEvent,
} from "@mui/material";

const SelectLanguage: React.FC = () => {
  const { i18n } = useTranslation();

  const languageChangeHandler = (event: SelectChangeEvent) => {
    language.changeLanguage(event.target.value);
  };

  return (
    <Box sx={{ minWidth: 50, marginRight: 2 }}>
      <FormControl fullWidth variant="standard">
        <Select
          value={i18n.language}
          onChange={languageChangeHandler}
          color="primary"
        >
          <MenuItem value="ua">Ua</MenuItem>
          <MenuItem value="en">En</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
};

export default SelectLanguage;
