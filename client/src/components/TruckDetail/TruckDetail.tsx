import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useTranslation } from "react-i18next";
import {
  deleteUserTruckById,
  updateTruck,
} from "../../store/slices/trucksSlice";
import {
  Button,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
} from "@mui/material";
import { AppDispatch } from "../../store/store";
import { ITruck, TTypeOfTruck } from "../../types/trucks";

interface ITruckSettingsParams {
  truck: ITruck;
}

const TruckSettings: React.FC<ITruckSettingsParams> = ({ truck }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();

  const [typeOfTruck, setTypeOfTruck] = useState<TTypeOfTruck>(truck.type);

  const isDifferentType = truck.type !== typeOfTruck;

  const radioButtonHandler = (event: any) => {
    setTypeOfTruck(event.target!.value);
  };

  const saveNewTypeHandler = () => {
    dispatch(updateTruck({ id: truck._id, type: typeOfTruck }));
  };

  const deleteTruckHandler = () => {
    dispatch(deleteUserTruckById(truck._id));
  };

  return (
    <Grid
      container
      alignItems="center"
      justifyContent="center"
      sx={{
        width: "100%",
        border: "1px solid #ccc",
        padding: 2,
      }}
    >
      <RadioGroup defaultValue={truck.type} row>
        <FormControlLabel
          value="SPRINTER"
          control={<Radio />}
          label={t("sprinter")}
          onChange={radioButtonHandler}
        />
        <FormControlLabel
          value="SMALL STRAIGHT"
          control={<Radio />}
          label={t("smallStraight")}
          onChange={radioButtonHandler}
        />
        <FormControlLabel
          value="LARGE STRAIGHT"
          control={<Radio />}
          label={t("largeStraight")}
          onChange={radioButtonHandler}
        />
      </RadioGroup>
      <Button
        variant="outlined"
        disabled={!isDifferentType}
        onClick={saveNewTypeHandler}
      >
        {t("save")}
      </Button>
      <Button
        variant="outlined"
        onClick={deleteTruckHandler}
        sx={{ marginLeft: 1 }}
      >
        {t("delete")}
      </Button>
    </Grid>
  );
};

export default TruckSettings;
