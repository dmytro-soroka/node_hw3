import React from "react";
import moment from "moment";
import { useTranslation } from "react-i18next";
import { Typography, Grid } from "@mui/material";
import { ILoad } from "../../types/loads";

interface ILoadInfoParams {
  load: ILoad;
}

const LoadInfo: React.FC<ILoadInfoParams> = ({ load }) => {
  const { t } = useTranslation();

  const title = {
    fontWeight: "700",
    fontSize: 20,
    marginRight: 1,
  };

  return (
    <>
      <Typography
        sx={{
          fontSize: 30,
          fontWeight: 700,
          textAlign: "center",
          marginBottom: 5,
        }}
      >
        {t("loadInfo")}
      </Typography>
      <Grid container justifyContent="space-between">
        <Grid sx={{ width: "50%" }}>
          <Grid container alignItems="center">
            <Typography sx={title}>{t("name")}:</Typography>
            <Typography>{load.name}</Typography>
          </Grid>
          <Grid container alignItems="center">
            <Typography sx={title}>{t("createdBy")}:</Typography>
            <Typography>{load.created_by}</Typography>
          </Grid>
          <Grid container alignItems="center">
            <Typography sx={title}>{t("createdDate")}: </Typography>
            <Typography>
              {moment(load.created_date).format("DD:MM:YYYY HH:MM")}
            </Typography>
          </Grid>
          <Grid container alignItems="center">
            <Typography sx={title}>{t("payload")}: </Typography>
            <Typography>{load.payload}</Typography>
          </Grid>
          <Grid container alignItems="center">
            <Typography sx={title}>{t("dimensions")}:</Typography>
            <Typography>
              {load.dimensions.width}x{load.dimensions.height}x
              {load.dimensions.length}
            </Typography>
          </Grid>
          <Grid container alignItems="center">
            <Typography sx={title}>{t("status")}:</Typography>
            <Typography>{load.status}</Typography>
          </Grid>
          <Grid container alignItems="center">
            <Typography sx={title}>{t("state")}:</Typography>
            <Typography>{load.state ?? t("noAssigned")}</Typography>
          </Grid>
          <Grid container alignItems="center">
            <Typography sx={title}>{t("pickupAddress")}:</Typography>
            <Typography>{load.pickup_address}</Typography>
          </Grid>
          <Grid container alignItems="center">
            <Typography sx={title}>{t("deliveryAddress")}:</Typography>
            <Typography>{load.delivery_address}</Typography>
          </Grid>
          <Grid container alignItems="center">
            <Typography sx={title}>{t("assignedTo")}: </Typography>
            <Typography>{load.assigned_to ?? t("noAssigned")}</Typography>
          </Grid>
        </Grid>
        <Grid sx={{ width: "50%" }}>
          <Grid>
            <Typography sx={title}>{t("logs")}: </Typography>
            {load.logs.map((log) => (
              <Typography>{log.message}</Typography>
            ))}
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default LoadInfo;
