import React from "react";
import { useTranslation } from "react-i18next";
import { Box, Button, Modal, Typography } from "@mui/material";

interface ISubmitModalParams {
  isOpen: boolean;
  onPressOk: () => void;
  onClose: () => void;
}

const SubmitModal: React.FC<ISubmitModalParams> = ({
  isOpen,
  onPressOk,
  onClose,
}) => {
  const { t } = useTranslation();

  return (
    <Modal
      open={isOpen}
      onClose={onClose}
      sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}
    >
      <Box sx={{ bgcolor: "#fff", p: "20px", borderRadius: "10px" }}>
        <Typography variant="h5" textAlign="center" sx={{ mb: 2 }}>
          {t("youAreSure")}
        </Typography>
        <Box>
          <Button
            variant="outlined"
            onClick={onClose}
            sx={{ width: "45%", mr: 1 }}
          >
            {t("close")}
          </Button>
          <Button
            variant="contained"
            onClick={onPressOk}
            sx={{ width: "45%", ml: 1 }}
          >
            {t("confirm")}
          </Button>
        </Box>
      </Box>
    </Modal>
  );
};

export default SubmitModal;
