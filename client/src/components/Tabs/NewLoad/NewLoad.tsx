import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { addLoad, reset, selectLoads } from "../../../store/slices/loadsSlice";
import { AppDispatch } from "../../../store/store";
import { ILoadCreate } from "../../../types/loads";
import LoadForm from "../../LoadForm/LoadForm";

const NewLoad = () => {
  const dispatch = useDispatch<AppDispatch>();

  const { isSuccess, isError, message } = useSelector(selectLoads);

  useEffect(() => {
    if (isSuccess) toast.success(message);
    if (isError) toast.error(message);

    dispatch(reset());
  }, [isSuccess, isError, message, dispatch]);

  const submitCreateHandler = (loadData: ILoadCreate) => {
    dispatch(addLoad(loadData));
  };

  return <LoadForm onSubmit={submitCreateHandler} title="Create new load" />;
};

export default NewLoad;
