import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { logout, selectAuth } from "../../../store/slices/authSlice";
import { deleteUser } from "../../../store/slices/userSlice";
import { Grid, Box, Typography, Button, CircularProgress } from "@mui/material";
import SubmitModal from "../../SubmitModal/SubmitModal";
import ChangePassword from "../../ChangePassword/ChangePassword";
import { AppDispatch } from "../../../store/store";

const Profile = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();
  const navigate = useNavigate();

  const [isOpenEditPassword, setIsOpenEditPassword] = useState(false);
  const [isOpenDeleteAccount, setIsOpenDeleteAccount] = useState(false);

  const { user } = useSelector(selectAuth);

  const toggleChangePasswordHandler = () =>
    setIsOpenEditPassword((prevState) => !prevState);

  const toggleDeleteAccountHandler = () =>
    setIsOpenDeleteAccount((prevState) => !prevState);

  const deleteAccountHandler = () => {
    dispatch(deleteUser());
    dispatch(logout());
    navigate("/login");
  };

  return (
    <>
      <SubmitModal
        isOpen={isOpenDeleteAccount}
        onPressOk={deleteAccountHandler}
        onClose={toggleDeleteAccountHandler}
      />
      <Grid container justifyContent="center">
        <Box sx={{ width: "60%", mt: 6 }}>
          <Typography
            variant="h2"
            textAlign="center"
            sx={{
              fontFamily: "monospace",
              fontWeight: 700,
            }}
          >
            {t("hello")}😊
          </Typography>
          {user ? (
            <>
              {isOpenEditPassword ? (
                <ChangePassword onClose={toggleChangePasswordHandler} />
              ) : (
                <Grid container flexDirection="column" alignItems="center">
                  <Grid
                    item
                    xs={12}
                    sx={{ mt: 5, display: "center", justifyContent: "center" }}
                  >
                    <Button
                      variant="outlined"
                      onClick={toggleChangePasswordHandler}
                      sx={{ mr: 1 }}
                    >
                      {t("changePassword")}
                    </Button>
                    <Button
                      variant="outlined"
                      onClick={toggleDeleteAccountHandler}
                      sx={{ ml: 1 }}
                    >
                      {t("deleteAccount")}
                    </Button>
                  </Grid>
                </Grid>
              )}
            </>
          ) : (
            <Grid container justifyContent="center" sx={{ mt: 5 }}>
              <CircularProgress />
            </Grid>
          )}
        </Box>
      </Grid>
    </>
  );
};

export default Profile;
