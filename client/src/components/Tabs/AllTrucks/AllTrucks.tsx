import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import {
  assignTruck,
  getTrucks,
  reset,
  selectTrucks,
} from "../../../store/slices/trucksSlice";
import { Grid, Typography } from "@mui/material";
import TruckItem from "../../TruckItem/TruckItem";
import { AppDispatch } from "../../../store/store";

const AllTrucks: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();

  const [openedTruckId, setOpenedTruckId] = useState<string | null>(null);

  const { trucks, isNewlyLoading, isSuccess, isError, isLoading, message } =
    useSelector(selectTrucks);

  useEffect(() => {
    if (isNewlyLoading) dispatch(getTrucks());
  }, [dispatch, isNewlyLoading]);

  useEffect(() => {
    if (isSuccess) toast.success(message);
    if (isError) toast.error(message);

    dispatch(reset());
  }, [isSuccess, isError, message, dispatch]);

  const assignTruckHandler = (id: string) => {
    dispatch(assignTruck(id));
  };

  if (isLoading) return <Typography>{t("loading")}</Typography>;

  return (
    <Grid container justifyContent="space-between" sx={{ width: "100%" }}>
      <Grid
        container
        alignItems="center"
        sx={{
          width: "100%",
          border: "1px solid #ccc",
          padding: 2,
        }}
      >
        <Grid item sx={{ width: "25%" }}>
          {t("id")}
        </Grid>
        <Grid item sx={{ width: "25%" }}>
          {t("type")}
        </Grid>
        <Grid item sx={{ width: "25%" }}>
          {t("status")}
        </Grid>
      </Grid>
      {trucks.map((truck) => (
        <TruckItem
          truck={truck}
          onAssign={assignTruckHandler}
          openedTruckId={openedTruckId}
          setOpenedTruckId={setOpenedTruckId}
        />
      ))}
    </Grid>
  );
};

export default AllTrucks;
