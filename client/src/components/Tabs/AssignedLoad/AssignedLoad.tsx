import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import {
  getAssignedLoad,
  reset,
  selectLoads,
  setNextStepLoad,
} from "../../../store/slices/loadsSlice";
import { Button, Grid, Typography } from "@mui/material";
import LoadInfo from "../../LoadInfo/LoadInfo";
import { AppDispatch } from "../../../store/store";

const AssignedLoad = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();

  const { assignedLoad, isSuccess, isError, isNewlyGetAssignedLoad, message } =
    useSelector(selectLoads);

  useEffect(() => {
    if (isNewlyGetAssignedLoad) dispatch(getAssignedLoad());
  }, [isNewlyGetAssignedLoad, dispatch]);

  useEffect(() => {
    if (isSuccess) toast.success(message);
    if (isError) toast.error(message);

    dispatch(reset());
  }, [isSuccess, isError, message, dispatch]);

  const nextStepHandler = () => {
    dispatch(setNextStepLoad());
  };

  return (
    <>
      {assignedLoad ? (
        <Grid>
          <LoadInfo load={assignedLoad} />
          <Grid container justifyContent="center" sx={{ marginTop: 5 }}>
            <Button variant="outlined" onClick={nextStepHandler}>
              {t("nextStep")}
            </Button>
          </Grid>
        </Grid>
      ) : (
        <Typography
          variant="h5"
          sx={{ textAlign: "center", marginTop: 10, fontWeight: 700 }}
        >
          {t("noAssignedLoad")}
        </Typography>
      )}
    </>
  );
};

export default AssignedLoad;
