import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import { getLoads, reset, selectLoads } from "../../../store/slices/loadsSlice";
import { Grid, Typography } from "@mui/material";
import LoadFilter from "../../LoadFilter/LoadFilter";
import Loads from "../../Loads/Loads";
import { AppDispatch } from "../../../store/store";
import { TLoadStatus } from "../../../types/loads";

const AllLoads = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();

  const [filteredType, setFilteredType] = useState<TLoadStatus | "ALL">("ALL");

  const { loads, isNewlyGet, isSuccess, isError, isLoading, message } =
    useSelector(selectLoads);

  let filteredLoads = loads.filter((load) => load.status !== "SHIPPED");

  if (filteredType !== "ALL") {
    filteredLoads = loads.filter((load) => load.status === filteredType);
  }

  useEffect(() => {
    if (isNewlyGet) dispatch(getLoads());
  }, [dispatch, isNewlyGet]);

  useEffect(() => {
    if (isSuccess) toast.success(message);
    if (isError) toast.error(message);

    dispatch(reset());
  }, [isSuccess, isError, isLoading, message, dispatch]);

  if (isLoading) return <Typography>{t("loading")}</Typography>;

  return (
    <Grid>
      <LoadFilter
        filteredType={filteredType}
        setFilteredType={setFilteredType}
      />
      <Loads loads={filteredLoads} />
    </Grid>
  );
};

export default AllLoads;
