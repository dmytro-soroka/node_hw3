import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { getLoads, reset, selectLoads } from "../../../store/slices/loadsSlice";
import Loads from "../../Loads/Loads";
import { AppDispatch } from "../../../store/store";

const History = () => {
  const dispatch = useDispatch<AppDispatch>();

  const { loads, isNewlyGet, isSuccess, isError, message } =
    useSelector(selectLoads);

  useEffect(() => {
    if (isNewlyGet) dispatch(getLoads());
  }, [isNewlyGet, dispatch]);

  useEffect(() => {
    if (isSuccess) toast.success(message);
    if (isError) toast.error(message);

    dispatch(reset());
  }, [isSuccess, isError, message, dispatch]);

  const shippedLoads = loads.filter((load) => load.status === "SHIPPED");

  return <Loads loads={shippedLoads} />;
};

export default History;
