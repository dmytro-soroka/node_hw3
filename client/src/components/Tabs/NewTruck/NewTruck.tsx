import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { toast } from "react-toastify";
import {
  addTruck,
  reset,
  selectTrucks,
} from "../../../store/slices/trucksSlice";
import LoadingButton from "@mui/lab/LoadingButton";
import {
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
  Typography,
} from "@mui/material";
import { AppDispatch } from "../../../store/store";
import { TTypeOfTruck } from "../../../types/trucks";

const NewTruck = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();

  const [typeOfTruck, setTypeOfTruck] = useState<TTypeOfTruck>("SPRINTER");

  const { isLoading, isSuccess, isError, message } = useSelector(selectTrucks);

  useEffect(() => {
    if (isSuccess) toast.success(message);
    if (isError) toast.error(message);

    dispatch(reset());
  }, [isSuccess, isError, message, dispatch]);

  const radioButtonHandler = (event: any) => {
    setTypeOfTruck(event.target.value as TTypeOfTruck);
  };

  const createNewTruckHandler = () => {
    dispatch(addTruck(typeOfTruck));
  };

  return (
    <Grid container justifyContent="center" alignItems="center">
      <Grid
        container
        sx={{
          alignItems: "center",
          maxWidth: 500,
        }}
      >
        <Typography
          variant="h2"
          sx={{ display: "block", textAlign: "center", marginBottom: 2 }}
        >
          {t("createNewTruck")}
        </Typography>
        <RadioGroup defaultValue="SPRINTER" row>
          <FormControlLabel
            value="SPRINTER"
            control={<Radio />}
            label={t("sprinter")}
            onChange={radioButtonHandler}
          />
          <FormControlLabel
            value="SMALL STRAIGHT"
            control={<Radio />}
            label={t("smallStraight")}
            onChange={radioButtonHandler}
          />
          <FormControlLabel
            value="LARGE STRAIGHT"
            control={<Radio />}
            label={t("largeStraight")}
            onChange={radioButtonHandler}
          />
        </RadioGroup>
        <LoadingButton
          fullWidth
          variant="contained"
          color="secondary"
          loading={isLoading}
          sx={{ mt: 3, mb: 2 }}
          onClick={createNewTruckHandler}
        >
          {t("createNewTruck")}
        </LoadingButton>
      </Grid>
    </Grid>
  );
};

export default NewTruck;
