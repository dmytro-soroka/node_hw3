import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import { deleteLoad, editLoad, postLoad } from "../../store/slices/loadsSlice";
import { Button, Grid } from "@mui/material";
import LoadForm from "../LoadForm/LoadForm";
import LoadInfo from "../LoadInfo/LoadInfo";
import { AppDispatch } from "../../store/store";
import { ILoad, ILoadCreate } from "../../types/loads";

interface ILoadDetailParams {
  load: ILoad;
}

const LoadDetail: React.FC<ILoadDetailParams> = ({ load }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();

  const [isOpenEdit, setIsOpenEdit] = useState(false);

  const loadIsNoShipped = load.status !== "SHIPPED";
  const loadIsNew = load.status === "NEW";

  const deleteLoadHandler = () => {
    dispatch(deleteLoad(load._id));
  };

  const postLoadHandler = () => {
    dispatch(postLoad(load._id));
  };

  const toggleEditHandler = () => {
    setIsOpenEdit((prevState) => !prevState);
  };

  const submitEditHandler = (loadData: ILoadCreate) => {
    dispatch(editLoad({ id: load._id, data: loadData }));
    toggleEditHandler();
  };

  return (
    <Grid sx={{ padding: 5, border: "1px solid #ccc" }}>
      {isOpenEdit ? (
        <LoadForm
          load={load}
          onSubmit={submitEditHandler}
          onClose={toggleEditHandler}
          title="editData"
        />
      ) : (
        <>
          <LoadInfo load={load} />
          {loadIsNoShipped && (
            <Grid container justifyContent="center" sx={{ marginTop: 5 }}>
              <Button variant="outlined" onClick={toggleEditHandler}>
                {t("changeData")}
              </Button>
              <Button
                variant="outlined"
                onClick={deleteLoadHandler}
                sx={{ margin: "0 20px" }}
              >
                {t("deleteLoad")}
              </Button>
              {loadIsNew && (
                <Button variant="outlined" onClick={postLoadHandler}>
                  {t("postLoad")}
                </Button>
              )}
            </Grid>
          )}
        </>
      )}
    </Grid>
  );
};

export default LoadDetail;
