import React, { Dispatch, SetStateAction } from "react";
import moment from "moment";
import { useTranslation } from "react-i18next";
import { Button, Grid } from "@mui/material";
import LoadDetail from "../LoadDetail/LoadDetail";
import { ILoad } from "../../types/loads";

interface ILoadItemParams {
  load: ILoad;
  onDetailOpen: Dispatch<SetStateAction<string | null>>;
  openedLoadId: string | null;
}

const LoadItem: React.FC<ILoadItemParams> = ({
  load,
  onDetailOpen,
  openedLoadId,
}) => {
  const { t } = useTranslation();

  const isDetailOpened = openedLoadId === load._id;

  const openDetailHandler = () => {
    const newId = isDetailOpened ? null : load._id;
    onDetailOpen(newId);
  };

  return (
    <>
      <Grid
        container
        alignItems="center"
        sx={{
          width: "100%",
          border: "1px solid #ccc",
          padding: 2,
        }}
      >
        <Grid item sx={{ width: "20%" }}>
          {load.name}
        </Grid>
        <Grid item sx={{ width: "20%" }}>
          {moment(load.created_date).format("DD:MM:YYYY HH:MM")}
        </Grid>
        <Grid item sx={{ width: "20%" }}>
          {load.pickup_address}
        </Grid>
        <Grid item sx={{ width: "20%" }}>
          {load.delivery_address}
        </Grid>
        <Grid item sx={{ width: "20%" }}>
          <Button variant="outlined" onClick={openDetailHandler}>
            {isDetailOpened ? t("closeDetail") : t("openDetail")}
          </Button>
        </Grid>
      </Grid>
      {isDetailOpened && <LoadDetail load={load} />}
    </>
  );
};

export default LoadItem;
