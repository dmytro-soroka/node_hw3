import React, { Dispatch, SetStateAction } from "react";
import { useTranslation } from "react-i18next";
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
} from "@mui/material";
import { TLoadStatus } from "../../types/loads";

interface ILoadFilterParams {
  filteredType: TLoadStatus | "ALL";
  setFilteredType: Dispatch<SetStateAction<TLoadStatus | "ALL">>;
}

const LoadFilter: React.FC<ILoadFilterParams> = ({
  filteredType,
  setFilteredType,
}) => {
  const { t } = useTranslation();

  const selectChangeHandler = (event: SelectChangeEvent) => {
    setFilteredType(event.target.value as TLoadStatus | "ALL");
  };

  return (
    <FormControl sx={{ width: 200, marginBottom: 3 }}>
      <InputLabel>{t("loadStatus")}</InputLabel>
      <Select
        value={filteredType}
        label={t("loadStatus")}
        onChange={selectChangeHandler}
      >
        <MenuItem value="ALL">{t("all")}</MenuItem>
        <MenuItem value="NEW">{t("new")}</MenuItem>
        <MenuItem value="POSTED">{t("posted")}</MenuItem>
        <MenuItem value="ASSIGNED">{t("assigned")}</MenuItem>
      </Select>
    </FormControl>
  );
};

export default LoadFilter;
