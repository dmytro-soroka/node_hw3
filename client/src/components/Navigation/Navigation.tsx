import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useSelector, useDispatch } from "react-redux";
import { logout, selectAuth } from "../../store/slices/authSlice";
import {
  AppBar,
  Toolbar,
  Typography,
  Container,
  Button,
  Grid,
} from "@mui/material";
import SubmitModal from "../SubmitModal/SubmitModal";
import SelectLanguage from "../SelectLanguage/SelectLanguage";
import { IPage } from "../../types/pages";
import { AppDispatch } from "../../store/store";
import AdbIcon from "@mui/icons-material/Adb";

const Navigation = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();
  const navigate = useNavigate();

  const [isOpenModal, setIsOpenModal] = useState(false);

  const { user } = useSelector(selectAuth);

  const toggleLogoutHandler = () => setIsOpenModal((prevState) => !prevState);

  const logoutHandler = () => {
    toggleLogoutHandler();
    dispatch(logout());
    navigate("/login");
  };

  const navigateHandler = (page: IPage) => {
    if (page.onClick) {
      page.onClick();
    } else {
      navigate(page.path!);
    }
  };

  const pages: IPage[] = user
    ? [
        { name: "dashboard", path: "/" },
        { name: "logout", onClick: toggleLogoutHandler },
      ]
    : [
        { name: "signIn", path: "/login" },
        { name: "signUp", path: "/register" },
      ];

  return (
    <>
      <SubmitModal
        isOpen={isOpenModal}
        onPressOk={logoutHandler}
        onClose={toggleLogoutHandler}
      />
      <AppBar position="static">
        <Container>
          <Toolbar
            disableGutters
            sx={{ display: "flex", justifyContent: "space-between" }}
          >
            <Grid container alignItems="center">
              <AdbIcon sx={{ mr: 1 }} />
              <Typography
                variant="h6"
                sx={{
                  fontFamily: "monospace",
                  fontWeight: 700,
                }}
              >
                Loads App
              </Typography>
            </Grid>
            <Grid container justifyContent="flex-end" alignItems="center">
              {user && (
                <Typography sx={{ marginRight: 2 }}>{user.email}</Typography>
              )}
              <SelectLanguage />
              {pages.map((page) => (
                <Button
                  key={page.name}
                  sx={{ color: "#fff" }}
                  onClick={navigateHandler.bind(null, page)}
                >
                  {t(page.name)}
                </Button>
              ))}
            </Grid>
          </Toolbar>
        </Container>
      </AppBar>
    </>
  );
};
export default Navigation;
