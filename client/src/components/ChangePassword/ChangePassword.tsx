import React, { useEffect, FormEvent } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useTranslation } from "react-i18next";
import useInput from "../../hooks/use-input";
import { toast } from "react-toastify";
import {
  reset,
  selectUser,
  changePassword,
} from "../../store/slices/userSlice";
import isNotEmpty from "../../helpers/isNotEmpty";
import isConfirmedPassword from "../../helpers/isConfirmedPassword";
import { TextField, Button, Grid, Box } from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import { AppDispatch } from "../../store/store";

interface IChangePasswordParams {
  onClose: () => void;
}

const ChangePassword: React.FC<IChangePasswordParams> = ({ onClose }) => {
  const dispatch = useDispatch<AppDispatch>();
  const { t } = useTranslation();

  const { isLoading, isSuccess, isError, message } = useSelector(selectUser);

  useEffect(() => {
    if (isSuccess) {
      toast.success("Successfully change password!");
      onClose();
    }
    if (isError) toast.error(message);

    dispatch(reset());
  }, [isError, isSuccess, message, dispatch, onClose]);

  const {
    value: currentPasswordValue,
    valueChangeHandler: currentPasswordChangeHandler,
    reset: resetCurrentPassword,
  } = useInput(isNotEmpty);

  const {
    value: passwordValue,
    isValid: passwordIsValid,
    hasError: passwordHasError,
    valueChangeHandler: passwordChangeHandler,
    inputBlurHandler: passwordBlurHandler,
    reset: resetPassword,
  } = useInput(isNotEmpty);

  const {
    value: confirmPasswordValue,
    isValid: confirmPasswordIsValid,
    hasError: confirmPasswordHasError,
    valueChangeHandler: confirmPasswordChangeHandler,
    inputBlurHandler: confirmPasswordBlurHandler,
    reset: resetConfirmPassword,
  } = useInput(isConfirmedPassword(passwordValue));

  const formIsValid = passwordIsValid && confirmPasswordIsValid;

  const submitHandler = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (!formIsValid) return;

    const passwordsData = {
      oldPassword: currentPasswordValue,
      newPassword: passwordValue,
    };

    dispatch(changePassword(passwordsData));

    resetCurrentPassword();
    resetPassword();
    resetConfirmPassword();
  };

  const passwordLabel = passwordHasError
    ? "thePasswordCannotBeEmpty"
    : "password";
  const confirmPasswordLabel = confirmPasswordHasError
    ? "passwordsMustMatch"
    : "confirmPassword";

  return (
    <Grid container flexDirection="column" alignItems="center">
      <Box component="form" noValidate onSubmit={submitHandler} sx={{ mt: 3 }}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              required
              fullWidth
              type="password"
              label={t("oldPassword")}
              value={currentPasswordValue}
              onChange={currentPasswordChangeHandler}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              required
              fullWidth
              type="password"
              value={passwordValue}
              label={t(passwordLabel)}
              error={passwordHasError}
              onChange={passwordChangeHandler}
              onBlur={passwordBlurHandler}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              required
              fullWidth
              type="password"
              value={confirmPasswordValue}
              label={t(confirmPasswordLabel)}
              error={confirmPasswordHasError}
              onChange={confirmPasswordChangeHandler}
              onBlur={confirmPasswordBlurHandler}
            />
          </Grid>
        </Grid>
        <LoadingButton
          fullWidth
          type="submit"
          variant="contained"
          color="secondary"
          loading={isLoading}
          disabled={!formIsValid}
          sx={{ mt: 3, mb: 2 }}
        >
          {t("changePassword")}
        </LoadingButton>
        <Button fullWidth variant="contained" color="primary" onClick={onClose}>
          {t("cancel")}
        </Button>
      </Box>
    </Grid>
  );
};

export default ChangePassword;
