import React, { Dispatch, SetStateAction } from "react";
import { useTranslation } from "react-i18next";
import { Button, Grid } from "@mui/material";
import TruckSettings from "../TruckDetail/TruckDetail";
import { ITruck } from "../../types/trucks";

interface ITruckItemParams {
  truck: ITruck;
  onAssign: (id: string) => void;
  openedTruckId: string | null;
  setOpenedTruckId: Dispatch<SetStateAction<string | null>>;
}

const TruckItem: React.FC<ITruckItemParams> = ({
  truck,
  onAssign,
  openedTruckId,
  setOpenedTruckId,
}) => {
  const { t } = useTranslation();

  const isDetailOpened = openedTruckId === truck._id;
  const isTruckAssigned = !!truck.assigned_to;
  const isTruckWithLoad = truck.status === "OL";

  const openDetailHandler = () => {
    setOpenedTruckId(isDetailOpened ? null : truck._id);
  };

  return (
    <>
      <Grid
        container
        alignItems="center"
        sx={{
          width: "100%",
          border: "1px solid #ccc",
          padding: 2,
        }}
      >
        <Grid item sx={{ width: "25%" }}>
          {truck._id}
        </Grid>
        <Grid item sx={{ width: "25%" }}>
          {truck.type}
        </Grid>
        <Grid item sx={{ width: "25%" }}>
          {truck.status}
        </Grid>
        <Grid item sx={{ width: "25%" }}>
          <Button
            variant="outlined"
            disabled={isTruckAssigned}
            onClick={onAssign.bind(null, truck._id)}
          >
            {isTruckAssigned ? t("assigned") : t("assign")}
          </Button>
          {!isTruckWithLoad && (
            <Button
              variant="outlined"
              onClick={openDetailHandler}
              sx={{ marginLeft: 1 }}
            >
              {isDetailOpened ? t("closeSettings") : t("openSettings")}
            </Button>
          )}
        </Grid>
      </Grid>
      {isDetailOpened && <TruckSettings truck={truck} />}
    </>
  );
};

export default TruckItem;
