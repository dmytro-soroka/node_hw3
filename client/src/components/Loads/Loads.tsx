import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { Grid, Typography } from "@mui/material";
import LoadItem from "../LoadItem/LoadItem";
import { ILoad } from "../../types/loads";

interface ILoadsParams {
  loads: ILoad[];
}

const Loads: React.FC<ILoadsParams> = ({ loads }) => {
  const { t } = useTranslation();

  const [openedLoadId, setOpenedLoadId] = useState<null | string>(null);

  const hasLoads = loads.length > 0;

  return (
    <Grid>
      <Grid
        container
        alignItems="center"
        sx={{
          width: "100%",
          border: "1px solid #ccc",
          padding: 2,
        }}
      >
        <Grid item sx={{ width: "20%" }}>
          {t("loadName")}
        </Grid>
        <Grid item sx={{ width: "20%" }}>
          {t("createdDate")}
        </Grid>
        <Grid item sx={{ width: "20%" }}>
          {t("pickupAddress")}
        </Grid>
        <Grid item sx={{ width: "20%" }}>
          {t("deliveryAddress")}
        </Grid>
      </Grid>
      {hasLoads ? (
        loads.map((load) => (
          <LoadItem
            load={load}
            onDetailOpen={setOpenedLoadId}
            openedLoadId={openedLoadId}
          />
        ))
      ) : (
        <Grid
          container
          alignItems="center"
          justifyContent="center"
          sx={{
            width: "100%",
            border: "1px solid #ccc",
            padding: 2,
          }}
        >
          <Typography>{t("noLoads")}</Typography>
        </Grid>
      )}
    </Grid>
  );
};

export default Loads;
