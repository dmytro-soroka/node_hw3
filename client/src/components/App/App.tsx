import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { useSelector } from "react-redux";
import { selectAuth } from "../../store/slices/authSlice";
import Navigation from "../Navigation/Navigation";
import Dashboard from "../../pages/Dashboard/Dashboard";
import Register from "../../pages/Register/Register";
import Login from "../../pages/Login/Login";
import ForgotPassword from "../../pages/ForgotPassword/ForgotPassword";
import "react-toastify/dist/ReactToastify.css";

const App = () => {
  const { user } = useSelector(selectAuth);

  return (
    <BrowserRouter>
      <ToastContainer />
      <Navigation />
      <Routes>
        {user && <Route path="/" element={<Dashboard />} />}
        <Route path="/register" element={<Register />} />
        <Route path="/forgot-password" element={<ForgotPassword />} />
        <Route path="/login" element={<Login />} />
        <Route
          path="*"
          element={<Navigate to={user ? "/" : "/login"} replace />}
        />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
